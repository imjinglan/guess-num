﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
/*
 * 各位大佬讲究看看吧
 * QWQ 
 */
namespace GuessNum
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public int a = 3;
        public string b;
        public bool changeing = false;
        public bool guessinc = false;
        public bool issingle = true;
        public string GuessNuma;
        public int GuessNumb = 0;
        public int FinalNum=0;
        public int times = 0;
        public int maxn = 99999;
        private void program()
        {
            GuessBox.IsEnabled = false;
            ReStart.IsEnabled = false;
            NextNu.IsEnabled = false;
            GuessDownClick.IsEnabled = false;
            GuessUpCilck.IsEnabled = false;
            Notice.Text = "最高数字";
        }

        public MainWindow()
        {
            b = Convert.ToString(a);
            
            InitializeComponent();
            Numb.Text = b;

            program();

        }

        private void startrun()
        {
            times = 0;
            Steps.Text = Convert.ToString(times);
            Numa.Text = "≤X≤";
            LeftText.Text = "1";
            if (issingle)
                RightText.Text = Numb.Text;
            else
                RightText.Text = "99999";
            GuessBox.IsEnabled = true;
            ReStart.IsEnabled = true;
            Run.IsEnabled = false;
            GuessDownClick.IsEnabled = true;
            GuessUpCilck.IsEnabled = true;
            NextNu.IsEnabled = true;

        }
        private void UpCilck_Click(object sender, RoutedEventArgs e)
        {
            if (!issingle)
            {
                changeing = true;
                a++;
                b = Convert.ToString(a);
                Numb.Text = b;
                changeing = false;
            }
            else
            {
                changeing = true;
                a++;
                b = Convert.ToString(a);
                Numb.Text = b;
                changeing = false;
            }
        }

        private void DownClick_Click(object sender, RoutedEventArgs e)
        {
            
                changeing = true;
                if (a != 0)
                {
                    a--;

                }
                else a = 0;

                b = Convert.ToString(a);
                Numb.Text = b;
                changeing = false;
        }

        private void Numb_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (changeing) { }
            else
            {
                
                b = Numb.Text ;
                if (b == "") { a = 0; Numb.Text = Convert.ToString(a); }
                else
                { 
                    a = Convert.ToInt32(b);
                }
            }
        }

        private void Single_Checked(object sender, RoutedEventArgs e)
        {
            issingle = true;
        }

        private void Mulit_Checked(object sender, RoutedEventArgs e)
        {
            issingle = false;
            Notice.Text = "设定数字";
        }

        private void Run_Click(object sender, RoutedEventArgs e)
        {
            GuessBox.Text = "0";
            Great.Text = "";
            LeftText.Text = RightText.Text = "";
            Steps.Text = "0";
            if (issingle)
            {
                Random ran = new Random();
                int RandKey = ran.Next(1, Convert.ToInt32(Numb.Text));
                for (int i = 1; i <= 30; i++)
                {
                    ran = new Random();
                    RandKey = ran.Next(1, Convert.ToInt32(Numb.Text));
                    Numa.Text = RandKey.ToString();
                    Delay(20);
                }
                RandKey = ran.Next(1, Convert.ToInt32(Numb.Text));
                FinalNum = RandKey;
                CromeYe.Text = Convert.ToString(FinalNum);
                startrun();

            }
            else
            {
                changeing = true;
                FinalNum = Convert.ToInt32(Numb.Text);
                CromeYe.Text = Numb.Text;
                Numb.Text = "";
                Numb.IsEnabled = false;
                UpCilck.IsEnabled = false;
                DownClick.IsEnabled = false;
                GuessBox.Text = "0";
                startrun();
            }
        }

        private void Delay(int Millisecond) //延迟系统时间，但系统又能同时能执行其它任务；
        {
            DateTime current = DateTime.Now;
            while (current.AddMilliseconds(Millisecond) > DateTime.Now)
            {
                System.Windows.Forms.Application.DoEvents();//转让控制权            
            }
            return;
        }

        private void ReStart_Click(object sender, RoutedEventArgs e)
        {
            Great.Text = "";
            LeftText.Text = RightText.Text = "";
            GuessBox.Text = "0";
            Steps.Text = "0";
            if (issingle)
            {
                Random ran = new Random();
                int RandKey = ran.Next(1, Convert.ToInt32(Numb.Text));
                for (int i = 1; i <= 30; i++)
                {
                    ran = new Random();
                    RandKey = ran.Next(1, Convert.ToInt32(Numb.Text));
                    Numa.Text = RandKey.ToString();
                    Delay(20);
                }
                RandKey = ran.Next(1, Convert.ToInt32(Numb.Text));
                FinalNum = RandKey;
                startrun();

            }
            else
            {
                program();
                Run.IsEnabled = true;
                Mulit.IsChecked = true;
                issingle = false;
                Numb.IsEnabled = true;
                UpCilck.IsEnabled = true;
                DownClick.IsEnabled = true;
            }
        }

        private void NextNu_Click(object sender, RoutedEventArgs e)
        { 
            times++;
            Steps.Text = Convert.ToString(times);
            if (GuessNumb > FinalNum)
            {
                Great.Text = "太大了！";
                RightText.Text = Convert.ToString(GuessNumb);
                GuessNuma = Convert.ToString(GuessNumb);
                GuessBox.Text = GuessNuma;
                Delay(300);
                Great.Text = "";
            }
            else 
            {
                if(GuessNumb < FinalNum)
                {
                    Great.Text = "太小了！";
                    LeftText.Text = Convert.ToString(GuessNumb);
                    GuessNuma = Convert.ToString(GuessNumb);
                    GuessBox.Text = GuessNuma;
                    Delay(300);
                    Great.Text = "";
                }
                if (GuessNumb == FinalNum)
                {
                    LeftText.Text = RightText.Text = "";
                    string temp = Convert.ToString(GuessNumb);
                        string tmp1 = "X=" + temp;
                    Numa.Text = tmp1;
                    Great.Text = "恭喜，猜对了！";
                    ReStart.IsEnabled = true;
                    times = 0;
                }
            }
        }

        private void GuessDownClick_Click(object sender, RoutedEventArgs e)
        {
            guessinc = true;
            if (GuessNumb != 0)
            {
                GuessNumb--;

            }
            else GuessNumb = 0;

            GuessNuma = Convert.ToString(GuessNumb);
            GuessBox.Text = GuessNuma;
            guessinc = false;
        }

        private void GuessUpClick_Click(object sender, RoutedEventArgs e)
        {
            guessinc = true;
            GuessNumb++;
            GuessNuma = Convert.ToString(GuessNumb);
            GuessBox.Text = GuessNuma;
            guessinc = false;
        }

        private void GuessBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (GuessBox.Text == "1145141")
                {
                    GuessBox.Text = Convert.ToString(FinalNum);
                }
            else { 
                if (guessinc) { }
                else
                {
                   GuessNuma = GuessBox.Text;
                   if (GuessNuma == "") { GuessNumb = 0; GuessBox.Text = Convert.ToString(GuessNumb); }
                   else
                   {
                       GuessNumb = Convert.ToInt32(GuessNuma);
                   }
                }
            }
        }

        private void Answer_Click(object sender, RoutedEventArgs e)
        {
            LeftText.Text = RightText.Text = "";
            string temp = Convert.ToString(FinalNum);
            string tmp1 = "X=" + temp;
            Numa.Text = tmp1;
            ReStart.IsEnabled = true;
            NextNu.IsEnabled = GuessUpCilck.IsEnabled = GuessDownClick.IsEnabled = GuessBox.IsEnabled = false;
        }
    }
}
